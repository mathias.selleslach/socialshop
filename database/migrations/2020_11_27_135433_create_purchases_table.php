<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    public function up(): void
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->uuid('uid');
            $table->date('date');
            $table->decimal('purchase_total');
            $table->integer('items_quantity');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
