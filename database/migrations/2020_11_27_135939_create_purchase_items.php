<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseItems extends Migration
{
    public function up(): void
    {
        Schema::create('purchase_items', function (Blueprint $table) {
            $table->id();
            $table->uuid('uid');
            $table->string('name');
            $table->text('description');
            $table->decimal('unit_price');
            $table->decimal('total_price');
            $table->integer('quantity');

            $table->timestamps();
            $table->softDeletes();

            $table->foreignId('product_id')->nullable()->constrained('products')->onDelete('set null');
            $table->foreignId('purchase_id')->constrained('purchases')->onDelete('cascade');
        });
    }
}
