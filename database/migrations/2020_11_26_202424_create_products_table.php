<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->uuid('uid');
            $table->string('name');
            $table->text('description');
            $table->decimal('price');
            $table->integer('stock')->default(0);
            $table->logUserInteraction();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
