<?php

return [
    'name' => env('COOKIE_NAME', 'Laravel'),
    'domain' => env('COOKIE_DOMAIN', 'localhost'),
    'secure' => env('COOKIE_SECURE', false),
];
