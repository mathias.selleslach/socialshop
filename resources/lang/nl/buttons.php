<?php

return [
    'login' => 'Aanmelden',
    'register' => 'Registreren',
    'register-here' => 'Registreer je hier!',
    'forgot-password' => 'Wachtwoord vergeten?',
    'reset-password' => 'Verzend wachtwoord herstel link',
    'reset' => 'Wijzig',
    'request-another' => 'Klik hier om het opnieuw te verzenden',
];
