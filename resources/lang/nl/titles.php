<?php

return [
    'login' => 'Aanmelden',
    'sign-up' => 'Registreren',
    'forgot-password' => 'Wachtwoord vergeten',
    'reset-password' => 'Wachtwoord opnieuw instellen',
    'verify-email-address' => 'Bevestig jouw e-mailadres',
    'confirm-password' => 'Wachtwoord bevestigen',
    'confirm' => 'Bevestigen',
];
