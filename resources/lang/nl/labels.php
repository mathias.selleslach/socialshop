<?php

return [
    'name' => 'Naam',
    'email' => 'E-mailadres',
    'password' => 'Wachtwoord',
    'password-confirmation' => 'Wachtwoord bevestigen',
    'not-registered' => 'Heb je nog geen account?',
    'already-registered' => 'Heb je al een account?',
    'remember-password' => 'Herinner je je wachtwoord plots terug?',
    'changed-your-mind' => 'Van gedachten veranderd?',
];
