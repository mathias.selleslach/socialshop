<?php

return [
    'login' => 'Login',
    'sign-up' => 'Sign up',
    'forgot-password' => 'Forgot password',
    'reset-password' => 'Reset password',
    'verify-email-address' => 'Verify your email address',
    'confirm-password' => 'Confirm password',
    'confirm' => 'Confirm',
];
