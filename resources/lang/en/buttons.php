<?php

return [
    'login' => 'Login',
    'register' => 'Sign up',
    'register-here' => 'Sign up here!',
    'forgot-password' => 'Forgot password?',
    'reset-password' => 'Send password reset link',
    'reset' => 'Reset',
    'request-another' => 'Click here to request another',
];
