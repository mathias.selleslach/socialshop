<?php

return [
    'name' => 'Name',
    'email' => 'Email address',
    'password' => 'Password',
    'password-confirmation' => 'Confirm password',
    'not-registered' => 'Don\'t have an account?',
    'already-registered' => 'Already have an account?',
    'remember-password' => 'Suddenly remember your password?',
    'changed-your-mind' => 'Did you change your mind?',
];
