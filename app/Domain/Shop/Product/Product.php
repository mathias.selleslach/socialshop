<?php

namespace App\Domain\Shop\Product;

use App\Infrastructure\Model;

class Product extends Model
{
    protected $casts =  [
        'stock' => 'integer',
        'price' => 'float'
    ];
}
