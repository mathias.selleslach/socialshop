<?php

namespace App\Domain\Shop\Product;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    /**
     * @inheritDoc
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->sentence(5),
            'price' => $this->faker->randomFloat(2, 1, 99.95),
            'stock' => $this->faker->numberBetween(1, 99),
        ];
    }
}
