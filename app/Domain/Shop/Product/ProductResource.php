<?php

namespace App\Domain\Shop\Product;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Product
 * @extends JsonResource<Product>
 */
class ProductResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'uid' => $this->uid,
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price,
            'stock' => $this->stock,
        ];
    }
}
