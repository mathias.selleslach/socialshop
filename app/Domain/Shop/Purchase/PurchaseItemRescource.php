<?php

namespace App\Domain\Shop\Purchase;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin PurchaseItem
 * @extends JsonResource<PurchaseItem>
 */

class PurchaseItemRescource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'uid' => $this->uid,
            'name' => $this->name,
            'description' => $this->description,
            'unit_price' => $this->unit_price,
            'total_price' => $this->total_price,
            'quantity' => $this->quantity,
        ];
    }
}
