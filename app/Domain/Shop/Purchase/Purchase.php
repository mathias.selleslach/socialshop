<?php

namespace App\Domain\Shop\Purchase;

use App\Infrastructure\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Purchase extends Model
{
    protected $casts = [
        'date' => 'datetime',
        'purchase_total' => 'float',
        'items_quantity' => 'integer',
    ];

    public function purchaseItems(): HasMany
    {
        return $this->hasMany(PurchaseItem::class);
    }
}
