<?php

namespace App\Domain\Shop\Purchase;

use Illuminate\Database\Eloquent\Factories\Factory;

class PurchaseFactory extends Factory
{
    protected $model = Purchase::class;

    /**
     * @inheritDoc
     */
    public function definition()
    {
        return [
            'date' => $this->faker->date(),
        ];
    }
}
