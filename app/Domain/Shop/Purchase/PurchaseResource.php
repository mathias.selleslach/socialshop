<?php

namespace App\Domain\Shop\Purchase;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Purchase
 * @extends JsonResource<Purchase>
 */
class PurchaseResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'uid' => $this->uid,
            'date' => $this->date->format('d-m-Y'),
            'purchase_total' => $this->purchase_total,
            'items_quantity' => $this->items_quantity,
            'items' => PurchaseItemRescource::collection($this->purchaseItems),
        ];
    }
}
