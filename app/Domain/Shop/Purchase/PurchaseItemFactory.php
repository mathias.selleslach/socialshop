<?php

namespace App\Domain\Shop\Purchase;

use Illuminate\Database\Eloquent\Factories\Factory;

class PurchaseItemFactory extends Factory
{
    protected $model = PurchaseItem::class;

    /**
     * @inheritDoc
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'description' => $this->faker->sentence,
            'unit_price' => $this->faker->randomFloat(2, 1, 99),
            'total_price' => $this->faker->randomFloat(2, 1, 99),
            'quantity' => $this->faker->numberBetween(1, 99),
        ];
    }
}
