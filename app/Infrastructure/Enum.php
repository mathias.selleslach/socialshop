<?php

namespace App\Infrastructure;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;
use JsonSerializable;
use ReflectionClass;
use function collect;
use function trans;

/**
 * Class Enum.
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class Enum implements Arrayable, JsonSerializable
{
    protected static array $enumMapping = [];

    protected string $name;

    /** @var int|string */
    protected $value;

    final private function __construct(string $name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /** @throws InvalidArgumentException Thrown when the value doesn't represent an enum instance. */
    public static function fromValue($value): self
    {
        $instances = static::getInstances();

        if (!isset($instances[$value])) {
            throw new InvalidArgumentException(trans(
                'errors.enum.invalid_enum_value',
                ['class' => static::class, 'value' => $value]
            ));
        }

        return $instances[$value];
    }

    /** @throws ValidationException */
    public static function fromName(?string $name): self
    {
        $instances = static::getInstancesByName();
        $name = Str::lower($name);

        if (!isset($instances[$name])) {
            $message = trans('errors.enum.invalid_enum_name', ['class' => static::class, 'name' => $name]);
            throw ValidationException::withMessages(['enum' => $message]);
        }

        return $instances[$name];
    }

    /**
     * @SuppressWarnings(PHPMD.UndefinedVariable)
     * @see https://github.com/phpmd/phpmd/issues/714
     */
    protected static function initializeMapping(): void
    {
        $className = static::class;

        if (!isset(static::$enumMapping[$className])) {
            $reflectionClass = new ReflectionClass($className);
            $constants = $reflectionClass->getConstants();

            static::$enumMapping[$className] = [
                'names' => [],
                'values' => [],
            ];

            foreach ($constants as $name => $value) {
                $name = Str::lower($name);
                $enum = new static($name, $value);
                static::$enumMapping[$className]['names'][$name] = $enum;
                static::$enumMapping[$className]['values'][$value] = $enum;
            }
        }
    }

    /** @return int|string */
    public function getValue()
    {
        return $this->value;
    }

    public function getKey(): int
    {
        return $this->value;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getHumanReadableName(): string
    {
        return trans($this->getTranslationKey());
    }

    public function getTranslationKey(): string
    {
        return 'enums.' . Str::kebab(class_basename(static::class)) . '.' . Str::lower($this->getName());
    }

    public function toArray(): array
    {
        return [
            'value' => $this->getValue(),
            'name' => $this->getHumanReadableName(),
        ];
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    public static function getValues(): array
    {
        return array_keys(static::getInstances());
    }

    public static function getNames(): array
    {
        return array_keys(static::getInstancesByName());
    }

    /**
     * @SuppressWarnings(PHPMD.UndefinedVariable)
     * @see https://github.com/phpmd/phpmd/issues/714
     */
    protected static function getAllInstances(): array
    {
        $className = static::class;
        if (!isset(static::$enumMapping[$className])) {
            static::initializeMapping();
        }

        return static::$enumMapping[$className];
    }

    /** @return static[] */
    public static function getInstancesByName(): array
    {
        return static::getAllInstances()['names'];
    }

    /** @return static[] */
    public static function getInstances(): array
    {
        return static::getAllInstances()['values'];
    }

    public static function getSelectFields(): Collection
    {
        return collect(static::getInstances())
            ->map(function (self $instance) {
                return [
                    'text' => $instance->getHumanReadableName(),
                    'value' => $instance->getValue(),
                ];
            })
            ->values();
    }

    public static function __callStatic(string $method, array $parameters): self
    {
        return static::fromName(Str::snake($method));
    }

    /** Recreate the enum after serialization. */
    public static function __set_state(array $attributes): self
    {
        return static::fromValue($attributes['value']);
    }

    /** @return int|string */
    public function jsonSerialize()
    {
        return $this->getValue();
    }
}
