<?php

use Ramsey\Uuid\Uuid;

/**
 * Generate a uuid4.
 *
 * @return string uuid4.
 */
function uuid(): string
{
    return Uuid::uuid4()->toString();
}
