<?php

use Illuminate\Database\Schema\Blueprint;

/*
 * Add the updater_id and creator_id column to the table.
 *
 * @return void
 */

// @codeCoverageIgnoreStart
Blueprint::macro(
    'logUserInteraction',
    // @codeCoverageIgnoreEnd
    function (): void {
        $this->bigInteger('creator_id')->unsigned()->nullable();
        $this->bigInteger('updater_id')->unsigned()->nullable();
        $this->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
        $this->foreign('updater_id')->references('id')->on('users')->onDelete('set null');
    }
);
