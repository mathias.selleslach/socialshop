<?php

namespace App\Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function register(): void
    {
        require_once __DIR__ . '/Macros/blueprint.php';
    }
}
