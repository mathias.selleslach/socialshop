<?php

namespace App\Infrastructure\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /** @var string */
    protected $namespace = 'App\Application\Common\Controllers';

    /** @var string */
    public const HOME = '/';

    /** @return void */
    public function boot()
    {
        parent::boot();
    }

    public function map(): void
    {
        $this->mapApiRoutes();
    }

    protected function mapApiRoutes(): void
    {
        Route::domain('api.' . config('app.base_url'))
            ->group(base_path('routes/api.php'));
    }
}
