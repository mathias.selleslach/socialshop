<?php

namespace App\Infrastructure\Traits;

use Illuminate\Database\Eloquent\Model;

trait LogsUserInteraction
{
    /**
     * Uid field in the database.
     * Declared static because Laravel calls the ::creating statically.
     */
    protected static $updater_id = 'updater_id';
    protected static $creator_id = 'creator_id';

    /**
     * Initialize the trait. This method is called by the abstract Model during the 'boot' method.
     * The initialization will add listener on the 'creating' event,
     * so a uid can be inserted before the model is saved in the database.
     *
     * @return void
     */
    protected static function bootLogsUserInteraction(): void
    {
        static::creating(function ($model): void {
            self::setCreatorField($model);
        });
        static::updating(function ($model): void {
            self::setUpdaterField($model);
        });
    }

    /**
     * When a new model instance without uid is created, the uid field should be set automatically.
     *
     * @param Model $model The model to set the uid attribute on.
     *
     * @return void
     */
    protected static function setCreatorField(Model $model): void
    {
        if (empty($model->getAttribute(self::$creator_id))) {
            $model->setAttribute(static::$creator_id, auth()->user()->id);
        }
    }

    /**
     * When a new model instance without uid is created, the uid field should be set automatically.
     *
     * @param Model $model The model to set the uid attribute on.
     *
     * @return void
     */
    protected static function setUpdaterField(Model $model): void
    {
        $model->setAttribute(static::$updater_id, auth()->user()->id);
    }
}
