<?php

namespace App\Infrastructure\Traits;

use Illuminate\Database\Eloquent\Model;

trait Uid
{
    /**
     * Uid field in the database.
     * Declared static because Laravel calls the ::creating statically.
     */
    protected static $uid_field = 'uid';

    /**
     * Initialize the trait. This method is called by the abstract Model during the 'boot' method.
     * The initialization will add listener on the 'creating' event,
     * so a uid can be inserted before the model is saved in the database.
     *
     * @return void
     */
    protected static function bootUid(): void
    {
        static::creating(function ($model): void {
            self::setUidField($model);
        });
    }


    /**
     * When a new model instance without uid is created, the uid field should be set automatically.
     *
     * @param Model $model The model to set the uid attribute on.
     *
     * @return void
     */
    protected static function setUidField(Model $model): void
    {
        if (empty($model->getAttribute(self::$uid_field))) {
            // Object has no UID yet, generate one.
            $model->setAttribute(static::$uid_field, uuid());
        }
    }

    /**
     * Get the route key for the model.
     * Makes sure that we can bind based on UID.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uid';
    }
}
