<?php

namespace App\Infrastructure\Constants;

class Cookies
{
    public const HTTP_ONLY = true;
    public const RAW = false;
    public const SAME_SITE = 'Strict';
    public const PATH = null;
}
