<?php

namespace App\Infrastructure;

use App\Infrastructure\Exceptions\Factories\FactoryNotFoundException;
use App\Infrastructure\Traits\Uid;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * App\Infrastructure\Model
 *
 * @method static Builder|Model newModelQuery()
 * @method static Builder|Model newQuery()
 * @method static Builder|Model query()
 * @mixin Eloquent
 */
class Model extends BaseModel
{
    use HasFactory;
    use Uid;

    /**
     * @var array
     * Properties that represent enums.
     */
    protected $enums = [];


    public function setAttribute($key, $value): self
    {
        if (array_key_exists($key, $this->enums)) {
            $this->attributes[$key] = $value ? $value->getValue() : null;

            return $this;
        }
        return parent::setAttribute($key, $value);
    }

    public function getAttribute($key)
    {
        // Find out if the attribute we want to access the value for, is listed as an Enum.
        if (array_key_exists($key, $this->enums)) {
            // If there is no value set for this attribute, bail out. There is no point in spawning an Enum.
            if (!isset($this->attributes[$key])) {
                return null;
            }
            $attributeValue = $this->getAttributeFromArray($key);

            return !is_null($attributeValue) ?
                forward_static_call([$this->enums[$key], 'fromValue'], $attributeValue) :
                null;
        }
        return parent::getAttribute($key);
    }

    /**
     * @return Factory
     *
     * @throws FactoryNotFoundException when factory does not exist.
     */
    protected static function newFactory()
    {
        $factory = static::class . 'Factory';

        if (class_exists($factory) === false) {
            throw new FactoryNotFoundException($factory);
        }

        return $factory::new();
    }
}
