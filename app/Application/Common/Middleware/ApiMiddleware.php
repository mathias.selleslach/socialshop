<?php

namespace App\Application\Common\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

abstract class ApiMiddleware
{
    protected ?Request $request;

    public function handle($request, Closure $next, $guard = null)
    {
        $this->request = $request;
    }

    protected function abort(string $errorMessage = null, array $context = []): void
    {
        if ($errorMessage) {
            $this->error($errorMessage, $context);
        }

        abort(Response::HTTP_FORBIDDEN);
    }

    protected function error(string $errorMessage, array $context = []): void
    {
        $route = $this->request->route();

        $context = array_merge($context, [
            'Origin' => $this->request->header('Origin'),
            'IP Address' => $this->request->ip(),
            'Method' => $this->request->getMethod(),
            'Route' => $route ? $route->getName() : null,
        ]);

        Log::error($errorMessage, $context);
    }
}
