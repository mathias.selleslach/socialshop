<?php

namespace App\Application\Common\Middleware;

use App\Domain\Common\User\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\JWTAuth;

class AuthenticateApi extends ApiMiddleware
{
    protected JWTAuth $jwtAuth;

    public function __construct(JWTAuth $jwtAuth)
    {
        $this->jwtAuth = $jwtAuth;
    }

    /** @param Request $request */
    public function handle($request, Closure $next, $guard = null)
    {
        parent::handle($request, $next, $guard);

        if ($request->getMethod() === Request::METHOD_OPTIONS) {
            return $next($request);
        }

        $token = $request->cookie(config('cookies.name'));

        if ($token === null) {
            $this->abort('Cookie not set.');
        }

        try {
            $this->jwtAuth->setToken($token);
        } catch (TokenInvalidException $exception) {
            $this->abort('JWT Token invalid.', ['token' => $token]);
        }

        if ($this->jwtAuth->check() === false) {
            $this->error('JWT authentication check failed.', ['token' => $token]);

            abort(HttpResponse::HTTP_UNAUTHORIZED);
        }

        $user = User::whereUid($this->jwtAuth->getClaim('sub'))->first();

        if ($user === null) {
            $this->abort('User not found.', ['token' => $token, 'sub' => $this->jwtAuth->getClaim('sub')]);
        }

        Auth::setUser($user);

        return $next($request);
    }
}
