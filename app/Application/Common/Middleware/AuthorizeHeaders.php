<?php

namespace App\Application\Common\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthorizeHeaders extends ApiMiddleware
{
    /** @param Request $request */
    public function handle($request, Closure $next, $guard = null)
    {
        parent::handle($request, $next, $guard);

        if ($request->getMethod() === Request::METHOD_OPTIONS) {
            return $next($request);
        }

        if ($request->header('X-Requested-With') !== 'XMLHttpRequest') {
            $this->abort('Invalid Header X-Requested-With.');
        }

        $this->validateContentTypeHeader($request);

        if ($request->header('Accept') !== 'application/json') {
            $this->abort('Invalid Header Accept.');
        }

        return $next($request);
    }

    private function validateContentTypeHeader(Request $request): void
    {
        // Content-type validation does not work on GET requests
        if ($request->getMethod() !== Request::METHOD_POST && $request->getMethod() !== Request::METHOD_PUT) {
            return;
        }

        if ($request->header('Content-type') !== 'application/json') {
            $this->abort('Invalid Header Content-type.');
        }
    }
}
