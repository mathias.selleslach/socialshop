<?php

namespace App\Application\Common\Controllers\Auth;

use App\Infrastructure\Controller;
use App\Infrastructure\Constants\Cookies;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request): Response
    {
        $token = auth()->attempt($request->all([
            LoginRequest::EMAIL,
            LoginRequest::PASSWORD,
        ]));

        if ($token === false) {
            throw ValidationException::withMessages([
                'password' => trans('validation.password'),
            ]);
        }

        Cookie::queue(
            config('cookies.name'),
            $token,
            (int) auth()->factory()->getTTL(),
            Cookies::PATH,
            config('cookies.domain'),
            config('cookies.secure'),
            Cookies::HTTP_ONLY,
            Cookies::RAW,
            Cookies::SAME_SITE
        );

        return response()->noContent();
    }
}
