<?php

namespace App\Application\Shop\Controllers\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductIndexRequest extends FormRequest
{
    public const ORDER_BY = 'orderby';
    public const ORDER_DIRECTION = 'direction';

    public function rules(): array
    {
        return [
            self::ORDER_BY => [
                'nullable',
                Rule::in(['stock', 'price']),
            ],
            self::ORDER_DIRECTION => [
                'required_with:' . self::ORDER_BY,
                Rule::in(['asc', 'desc']),
            ],
        ];
    }
}
