<?php

namespace App\Application\Shop\Controllers\Product;

use App\Domain\Shop\Product\Product;
use App\Infrastructure\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductDeleteController extends Controller
{
    public function __invoke(Request $request, Product $product): Response
    {
        $product->delete();

        return response()->noContent();
    }
}
