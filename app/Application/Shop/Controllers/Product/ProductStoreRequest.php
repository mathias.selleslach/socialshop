<?php

namespace App\Application\Shop\Controllers\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    public const NAME = 'name';
    public const DESCRIPTION = 'description';
    public const PRICE = 'price';
    public const STOCK = 'stock';

    public function rules(): array
    {
        return [
            self::NAME => [
                'required',
                'string',
                'max:255',
            ],
            self::DESCRIPTION => [
                'required',
                'string',
                'max:65535',
            ],
            self::PRICE => [
                'required',
                'numeric'
            ],
            self::STOCK => [
                'required',
                'numeric',
            ]
        ];
    }
}
