<?php

namespace App\Application\Shop\Controllers\Product;

use App\Domain\Shop\Product\Product;
use App\Domain\Shop\Product\ProductResource;
use App\Infrastructure\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductIndexController extends Controller
{
    public function __invoke(ProductIndexRequest $request): AnonymousResourceCollection
    {
        $orderBy = $request->get(ProductIndexRequest::ORDER_BY, 'stock');
        $orderDirection = $request->get(ProductIndexRequest::ORDER_DIRECTION, 'desc');

        return ProductResource::collection(Product::orderBy($orderBy, $orderDirection)->get());
    }
}
