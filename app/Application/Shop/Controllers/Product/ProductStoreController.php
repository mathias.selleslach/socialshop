<?php

namespace App\Application\Shop\Controllers\Product;

use App\Domain\Shop\Product\Product;
use App\Domain\Shop\Product\ProductResource;
use App\Infrastructure\Controller;

class ProductStoreController extends Controller
{
    public function __invoke(ProductStoreRequest $request): ProductResource
    {
        $product = new Product();
        $product->name = $request->get(ProductStoreRequest::NAME);
        $product->description = $request->get(ProductStoreRequest::DESCRIPTION);
        $product->price = $request->get(ProductStoreRequest::PRICE);
        $product->stock = $request->get(ProductStoreRequest::STOCK);
        $product->creator_id = Auth()->user()->id;
        $product->save();

        return new ProductResource($product);
    }
}
