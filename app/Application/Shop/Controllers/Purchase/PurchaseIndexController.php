<?php

namespace App\Application\Shop\Controllers\Purchase;

use App\Domain\Shop\Purchase\Purchase;
use App\Domain\Shop\Purchase\PurchaseResource;
use App\Infrastructure\Controller;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PurchaseIndexController extends Controller
{
    public function __invoke(): AnonymousResourceCollection
    {
        return PurchaseResource::collection(Purchase::with('purchaseItems')->get());
    }
}
