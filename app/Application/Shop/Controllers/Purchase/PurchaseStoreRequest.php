<?php

namespace App\Application\Shop\Controllers\Purchase;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseStoreRequest extends FormRequest
{
    public const PRODUCTS = 'products';
    public const PRODUCT_UID = 'product_uid';
    public const PRODUCT_QUANTITY = 'product_quantity';

    public function rules(): array
    {
        return [
            self::PRODUCTS => [
                'required',
                'array',
            ],
            self::PRODUCTS . '.*.' . self::PRODUCT_UID => [
                'required',
                'uuid',
                'exists:products,uid',
            ],
            self::PRODUCTS . '.*.' . self::PRODUCT_QUANTITY => [
                'required',
                'numeric',
            ],
        ];
    }
}
