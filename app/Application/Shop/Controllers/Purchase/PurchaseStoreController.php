<?php

namespace App\Application\Shop\Controllers\Purchase;

use App\Domain\Shop\Product\Product;
use App\Domain\Shop\Purchase\Purchase;
use App\Domain\Shop\Purchase\PurchaseItem;
use App\Domain\Shop\Purchase\PurchaseResource;
use App\Infrastructure\Controller;
use Carbon\Carbon;

class PurchaseStoreController extends Controller
{
    public function __invoke(PurchaseStoreRequest $request)
    {
        $productUids = collect($request->get(PurchaseStoreRequest::PRODUCTS))->pluck('product_uid');
        $products = Product::whereIn('uid', $productUids)->get()->keyBy('uid');

        $purchase = new Purchase();
        $purchase->date = Carbon::now();
        $purchase->items_quantity = 0;
        $purchase->purchase_total = 0;
        $purchase->save();

        $purchaseTotal = 0;
        $purchaseQuantityTotal = 0;
        $purchaseItems = [];

        foreach ($request->get(PurchaseStoreRequest::PRODUCTS) as $productData) {
            $product = $products->get($productData[PurchaseStoreRequest::PRODUCT_UID]);
            $purchaseItem = new PurchaseItem();
            $purchaseItem->name = $product->name;
            $purchaseItem->description = $product->description;
            $purchaseItem->unit_price = $product->price;
            $purchaseItem->total_price = $product->price * $productData[PurchaseStoreRequest::PRODUCT_QUANTITY];
            $purchaseItem->quantity = $productData[PurchaseStoreRequest::PRODUCT_QUANTITY];
            $purchaseItem->product_id = $product->id;
            $purchaseItem->purchase_id = $purchase->id;
            $purchaseItem->save();

            $purchaseQuantityTotal += $purchaseItem->quantity;
            $purchaseTotal += $purchaseItem->total_price;
            $purchaseItems[] = $purchaseItem;
        }

        $purchase->items_quantity = $purchaseQuantityTotal;
        $purchase->purchase_total = $purchaseTotal;
        $purchase->setRelation('purchaseItems', $purchaseItems);
        $purchase->save();

        return new PurchaseResource($purchase);
    }
}
