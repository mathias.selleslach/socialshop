<?php

use App\Application\Common\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Version 1
|--------------------------------------------------------------------------
*/

Route::middleware(['api'])->prefix('v1')->group(function (): void {
    require 'api/v1.php';
});

Route::post('/login', LoginController::class)
    ->name('api.login');
