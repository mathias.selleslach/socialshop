<?php

use App\Application\Common\Middleware\AuthenticateApi;
use App\Application\Shop\Controllers\Product\ProductDeleteController;
use App\Application\Shop\Controllers\Product\ProductIndexController;
use App\Application\Shop\Controllers\Product\ProductStoreController;
use App\Application\Shop\Controllers\Product\ProductUpdateController;
use App\Application\Shop\Controllers\Purchase\PurchaseIndexController;
use App\Application\Shop\Controllers\Purchase\PurchaseStoreController;
use Illuminate\Support\Facades\Route;

// All routes that are only accessible after login (Users)
Route::middleware([AuthenticateApi::class])->group(function (): void {
    Route::post('product', ProductStoreController::class)->name('api.product.store');
    Route::put('product/{product}', ProductUpdateController::class)->name('api.product.update');
    Route::delete('product/{product}', ProductDeleteController::class)->name('api.product.delete');
    Route::get('purchases', PurchaseIndexController::class)->name('api.purchases.index');
});

// Client routes that do not require authentication.

Route::get('products', ProductIndexController::class)->name('api.product.index');
Route::post('purchase', PurchaseStoreController::class)->name('api.purchase.store');
