<?php

namespace Tests\Unit\Application\Shop\Controllers\Product;

use App\Application\Shop\Controllers\Product\ProductIndexRequest;
use App\Application\Shop\Controllers\Product\ProductStoreRequest;
use App\Domain\Shop\Product\Product;
use Illuminate\Http\Request;
use Tests\Unit\ControllerTestCase;

class ProductIndexControllerTest extends ControllerTestCase
{
    protected string $routeName = 'api.product.index';

    protected string $method = Request::METHOD_GET;

    protected ?Product $product;
    protected ?Product $otherProduct;

    public function setUp(): void
    {
        parent::setUp();

        $this->product = Product::factory()->create(['stock' => 1, 'price' => 9.99]);
        $this->otherProduct = Product::factory()->create(['stock' => 5, 'price' => 1]);
    }

    /** @test */
    public function it_returns_the_products_ordered_by_stock_desc_when_no_filters_are_present(): void
    {
        $response = $this->execute()->assertOk()->json();

        self::assertCount(2, $response['data']);
        self::assertEquals($this->otherProduct->uid, $response['data'][0]['uid']);
        self::assertEquals($this->product->uid, $response['data'][1]['uid']);
    }

    /** @test */
    public function it_returns_the_products_ordered_by_stock_desc_when_filtered_that_way(): void
    {
        $this->setUrlParameters([
            ProductIndexRequest::ORDER_BY => 'stock',
            ProductIndexRequest::ORDER_DIRECTION => 'desc'
        ]);
        $response = $this->execute()->assertOk()->json();

        self::assertCount(2, $response['data']);
        self::assertEquals($this->otherProduct->uid, $response['data'][0]['uid']);
        self::assertEquals($this->product->uid, $response['data'][1]['uid']);
    }

    /** @test */
    public function it_returns_the_products_ordered_by_stock_asc_when_filtered_that_way(): void
    {
        $this->setUrlParameters([
            ProductIndexRequest::ORDER_BY => 'stock',
            ProductIndexRequest::ORDER_DIRECTION => 'asc'
        ]);
        $response = $this->execute()->assertOk()->json();

        self::assertCount(2, $response['data']);
        self::assertEquals($this->product->uid, $response['data'][0]['uid']);
        self::assertEquals($this->otherProduct->uid, $response['data'][1]['uid']);
    }

    /** @test */
    public function it_returns_the_products_ordered_by_price_desc_when_filtered_that_way(): void
    {
        $this->setUrlParameters([
            ProductIndexRequest::ORDER_BY => 'price',
            ProductIndexRequest::ORDER_DIRECTION => 'desc'
        ]);
        $response = $this->execute()->assertOk()->json();

        self::assertCount(2, $response['data']);
        self::assertEquals($this->product->uid, $response['data'][0]['uid']);
        self::assertEquals($this->otherProduct->uid, $response['data'][1]['uid']);
    }

    /** @test */
    public function it_returns_the_products_ordered_by_price_asc_when_filtered_that_way(): void
    {
        $this->setUrlParameters([
            ProductIndexRequest::ORDER_BY => 'price',
            ProductIndexRequest::ORDER_DIRECTION => 'asc',
        ]);
        $response = $this->execute()->assertOk()->json();

        self::assertCount(2, $response['data']);
        self::assertEquals($this->otherProduct->uid, $response['data'][0]['uid']);
        self::assertEquals($this->product->uid, $response['data'][1]['uid']);
    }

    /** @test */
    public function it_returns_validation_error_when_order_direction_is_not_present(): void
    {
        $this->setUrlParameters([
            ProductIndexRequest::ORDER_BY => 'price',
        ]);

        $this->execute()->assertJsonValidationErrors([
            ProductIndexRequest::ORDER_DIRECTION => 'The direction field is required when orderby is present',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_order_direction_is_invalid_value(): void
    {
        $this->setUrlParameters([
            ProductIndexRequest::ORDER_BY => 'price',
            ProductIndexRequest::ORDER_DIRECTION => 'invalidValue'
        ]);

        $this->execute()->assertJsonValidationErrors([
            ProductIndexRequest::ORDER_DIRECTION => 'The selected direction is invalid',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_order_by_is_invalid_value(): void
    {
        $this->setUrlParameters([
            ProductIndexRequest::ORDER_BY => 'someInvalidThing',
            ProductIndexRequest::ORDER_DIRECTION => 'desc'
        ]);

        $this->execute()->assertJsonValidationErrors([
            ProductIndexRequest::ORDER_BY => 'The selected orderby is invalid',
        ]);
    }

    /** @test */
    public function it_allows_access_for_anonymous_user(): void
    {
        $this->actAsAnonymousUser()->execute()->assertOk();
    }

    /** @test */
    public function it_allows_access_for_authed_user(): void
    {
        $this->execute()->assertOk();
    }
}
