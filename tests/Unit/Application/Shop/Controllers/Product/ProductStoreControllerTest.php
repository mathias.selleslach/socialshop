<?php

namespace Tests\Unit\Application\Shop\Controllers\Product;

use App\Application\Shop\Controllers\Product\ProductStoreRequest;
use App\Domain\Shop\Product\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Tests\Unit\ControllerTestCase;

class ProductStoreControllerTest extends ControllerTestCase
{
    protected string $routeName = 'api.product.store';

    protected string $method = Request::METHOD_POST;

    /** @test */
    public function it_returns_validation_errors_without_data(): void
    {
        $this->execute([])->assertJsonValidationErrors([
            ProductStoreRequest::NAME => 'The name field is required',
            ProductStoreRequest::DESCRIPTION => 'The description field is required',
            ProductStoreRequest::PRICE => 'The price field is required',
            ProductStoreRequest::STOCK => 'The stock field is required',
        ]);
    }

    /** @test */
    public function it_returns_validation_errors_with_empty_data(): void
    {
        $data = [
            ProductStoreRequest::NAME => '',
            ProductStoreRequest::DESCRIPTION => '',
            ProductStoreRequest::PRICE => '',
            ProductStoreRequest::STOCK => '',
        ];

        $this->execute($data)->assertJsonValidationErrors([
            ProductStoreRequest::NAME => 'The name field is required',
            ProductStoreRequest::DESCRIPTION => 'The description field is required',
            ProductStoreRequest::PRICE => 'The price field is required',
            ProductStoreRequest::STOCK => 'The stock field is required',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_name_is_bigger_than_255_characters(): void
    {
        $data = $this->getData();
        $data[ProductStoreRequest::NAME] = Str::random(256);

        $this->execute($data)->assertJsonValidationErrors([
            ProductStoreRequest::NAME => 'The name may not be greater than 255 characters',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_description_is_bigger_than_65535_characters(): void
    {
        $data = $this->getData();
        $data[ProductStoreRequest::DESCRIPTION] = Str::random(65536);

        $this->execute($data)->assertJsonValidationErrors([
            ProductStoreRequest::DESCRIPTION => 'The description may not be greater than 65535 characters',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_price_is_not_numeric(): void
    {
        $data = $this->getData();
        $data[ProductStoreRequest::PRICE] = 'NaN';

        $this->execute($data)->assertJsonValidationErrors([
            ProductStoreRequest::PRICE => 'The price must be a number',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_stock_is_not_numeric(): void
    {
        $data = $this->getData();
        $data[ProductStoreRequest::STOCK] = 'NaN';

        $this->execute($data)->assertJsonValidationErrors([
            ProductStoreRequest::STOCK => 'The stock must be a number',
        ]);
    }

    /** @test */
    public function it_creates_the_product(): void
    {
        $this->execute()->assertCreated();

        $product = Product::orderByDesc('id')->first();

        self::assertEquals('My product', $product->name);
        self::assertEquals('My product description', $product->description);
        self::assertEquals(9.99, $product->price);
        self::assertEquals(1, $product->stock);
        self::assertEquals($this->user->id, $product->creator_id);
    }

    /** @test */
    public function it_returns_the_product_correctly(): void
    {
        $response = $this->execute()->assertCreated()->json();
        $product = Product::orderByDesc('id')->first();

        self::assertArrayHasKey('data', $response);
        self::assertCount(5, $response['data']);
        self::assertEquals([
            'uid' => $product->uid,
            'name' => $product->name,
            'description' => $product->description,
            'price' => 9.99,
            'stock' => 1,
        ], $response['data']);
    }

    /** @test */
    public function it_denies_access_for_anonymous_user(): void
    {
        $this->actAsAnonymousUser()->execute()->assertForbidden();
    }

    public function getData(): array
    {
        return [
            ProductStoreRequest::NAME => 'My product',
            ProductStoreRequest::DESCRIPTION => 'My product description',
            ProductStoreRequest::PRICE => 9.99,
            ProductStoreRequest::STOCK => 1,
        ];
    }
}
