<?php

namespace Tests\Unit\Application\Shop\Controllers\Product;

use App\Domain\Shop\Product\Product;
use Illuminate\Http\Request;
use Tests\Unit\ControllerTestCase;

class ProductDeleteControllerTest extends ControllerTestCase
{
    protected string $routeName = 'api.product.delete';

    protected string $method = Request::METHOD_DELETE;

    protected ?Product $product;

    public function setUp(): void
    {
        parent::setUp();

        $this->product = Product::factory()->create();

        $this->setUrlParameters(['product' => $this->product]);
    }
    /** @test */
    public function it_deletes_the_product(): void
    {
        $this->execute([])->assertNoContent();

        self::assertEmpty(Product::all());
    }

    /** @test */
    public function it_denies_access_for_anonymous_user(): void
    {
        $this->actAsAnonymousUser()->execute()->assertForbidden();
    }
}
