<?php

namespace Tests\Unit\Application\Shop\Controllers\Purchase;

use App\Application\Shop\Controllers\Purchase\PurchaseStoreRequest;
use App\Domain\Shop\Product\Product;
use App\Domain\Shop\Purchase\Purchase;
use Illuminate\Http\Request;
use Tests\Unit\ControllerTestCase;

class PurchaseStoreControllerTest extends ControllerTestCase
{
    protected string $routeName = 'api.purchase.store';

    protected string $method = Request::METHOD_POST;

    protected ?Product $product1;

    protected ?Product $product2;

    public function setUp(): void
    {
        parent::setUp();

        $this->product1 = Product::factory()->create(['price' => 10]);
        $this->product2 = Product::factory()->create(['price' => 15.95]);
    }

    /** @test */
    public function it_stores_the_purchase_correctly(): void
    {
        $response = $this->execute()->assertCreated()->json();
        $latestPurchase = Purchase::with('purchaseItems')->orderByDesc('id')->first();

        self::assertArrayHasKey('data', $response);
        self::assertEquals($latestPurchase->uid, $response['data']['uid']);
        self::assertEquals($latestPurchase->date->format('d-m-Y'), $response['data']['date']);
        self::assertEquals($latestPurchase->purchase_total, $response['data']['purchase_total']);
        self::assertEquals($latestPurchase->items_quantity, $response['data']['items_quantity']);
        self::assertCount(2, $response['data']['items']);

        // Assert first ordered product.
        self::assertEquals($latestPurchase->purchaseItems->first()->uid, $response['data']['items'][0]['uid']);
        self::assertEquals($latestPurchase->purchaseItems->first()->name, $response['data']['items'][0]['name']);
        self::assertEquals($latestPurchase->purchaseItems->first()->description, $response['data']['items'][0]['description']);
        self::assertEquals($latestPurchase->purchaseItems->first()->unit_price, $response['data']['items'][0]['unit_price']);
        self::assertEquals(5, $response['data']['items'][0]['quantity']);
        self::assertEquals(50, $response['data']['items'][0]['total_price']);

        // Assert second ordered product
        self::assertEquals($latestPurchase->purchaseItems->last()->uid, $response['data']['items'][1]['uid']);
        self::assertEquals($latestPurchase->purchaseItems->last()->name, $response['data']['items'][1]['name']);
        self::assertEquals($latestPurchase->purchaseItems->last()->description, $response['data']['items'][1]['description']);
        self::assertEquals($latestPurchase->purchaseItems->last()->unit_price, $response['data']['items'][1]['unit_price']);
        self::assertEquals(2, $response['data']['items'][1]['quantity']);
        self::assertEquals(31.9, $response['data']['items'][1]['total_price']);

        // Also assert the hardcoded total & quantity, to make sure that calculation checks out.
        self::assertEquals(81.9, $latestPurchase->purchase_total);
        self::assertEquals(7, $latestPurchase->items_quantity);
    }

    /** @test */
    public function it_returns_validation_errors_without_data(): void
    {
        $this->execute([])->assertJsonValidationErrors([
            PurchaseStoreRequest::PRODUCTS => 'The products field is required',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_products_is_not_an_array(): void
    {
        $this->execute([PurchaseStoreRequest::PRODUCTS => 'Not An Array'])->assertJsonValidationErrors([
            PurchaseStoreRequest::PRODUCTS => 'The products must be an array',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_product_uid_is_not_a_uuid(): void
    {
        $data = $this->getData();
        $data[PurchaseStoreRequest::PRODUCTS][0][PurchaseStoreRequest::PRODUCT_UID] = 'Not a uuid';
        $this->execute($data)->assertJsonValidationErrors([
            PurchaseStoreRequest::PRODUCTS.'.0.'.PurchaseStoreRequest::PRODUCT_UID =>
                'The products.0.product_uid must be a valid UUID',
        ]);
    }

    /** @test */
    public function it_returns_a_validation_error_if_the_product_uuid_no_longer_exists_in_the_database(): void
    {
        $data = $this->getData();
        $this->product1->delete();
        $this->execute($data)->assertJsonValidationErrors([
            PurchaseStoreRequest::PRODUCTS.'.0.'.PurchaseStoreRequest::PRODUCT_UID =>
                'The selected products.0.product_uid is invalid',
        ]);
    }

    /** @test */
    public function it_returns_a_validation_error_if_the_product_quantity_is_missing(): void
    {
        $data = $this->getData();
        unset($data[PurchaseStoreRequest::PRODUCTS][0][PurchaseStoreRequest::PRODUCT_QUANTITY]);
        $this->execute($data)->assertJsonValidationErrors([
            PurchaseStoreRequest::PRODUCTS.'.0.'.PurchaseStoreRequest::PRODUCT_QUANTITY =>
                'The products.0.product_quantity field is required',
        ]);
    }

    /** @test */
    public function it_returns_a_validation_error_if_the_product_quantity_is_not_numeric(): void
    {
        $data = $this->getData();
        $data[PurchaseStoreRequest::PRODUCTS][0][PurchaseStoreRequest::PRODUCT_QUANTITY] = 'NaN';
        $this->execute($data)->assertJsonValidationErrors([
            PurchaseStoreRequest::PRODUCTS.'.0.'.PurchaseStoreRequest::PRODUCT_QUANTITY =>
                'The products.0.product_quantity must be a number',
        ]);
    }

    public function getData(): array
    {
        return [
            PurchaseStoreRequest::PRODUCTS => [
                [
                    'product_uid' => $this->product1->uid,
                    'product_quantity' => 5
                ],
                [
                    'product_uid' => $this->product2->uid,
                    'product_quantity' => 2
                ]
            ]
        ];
    }
}
