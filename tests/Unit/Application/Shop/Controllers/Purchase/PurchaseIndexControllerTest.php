<?php

namespace Tests\Unit\Application\Shop\Controllers\Purchase;

use App\Domain\Shop\Product\Product;
use App\Domain\Shop\Purchase\Purchase;
use App\Domain\Shop\Purchase\PurchaseItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tests\Unit\ControllerTestCase;

class PurchaseIndexControllerTest extends ControllerTestCase
{
    protected string $routeName = 'api.purchases.index';

    protected string $method = Request::METHOD_GET;

    protected ?Product $product1;

    protected ?Product $product2;

    protected ?Purchase $purchase;

    protected ?PurchaseItem $purchaseItem1;

    protected ?PurchaseItem $purchaseItem2;

    public function setUp(): void
    {
        parent::setUp();

        $this->product1 = Product::factory()->create(['price' => 10]);
        $this->product2 = Product::factory()->create(['price' => 15.95]);

        $this->purchase = Purchase::factory()->create(['purchase_total' => 25.95, 'items_quantity' => 2]);
        $this->purchaseItem1 = PurchaseItem::factory()->create([
            'name' => $this->product1->name,
            'description' => $this->product1->description,
            'unit_price' => $this->product1->price,
            'total_price' => $this->product1->price,
            'quantity' => 1,
            'purchase_id' => $this->purchase->id,
        ]);
        $this->purchaseItem2 = PurchaseItem::factory()->create([
            'name' => $this->product2->name,
            'description' => $this->product2->description,
            'unit_price' => $this->product2->price,
            'total_price' => $this->product2->price,
            'quantity' => 1,
            'purchase_id' => $this->purchase->id,
        ]);
    }

    /** @test */
    public function it_returns_the_purchases_correctly(): void
    {
        $response = $this->execute()->assertOk()->json();

        self::assertArrayHasKey('data', $response);
        self::assertCount(1, $response['data']);
        self::assertEquals($this->purchase->uid, $response['data'][0]['uid']);
        self::assertEquals($this->purchase->date->format('d-m-Y'), $response['data'][0]['date']);
        self::assertEquals($this->purchase->purchase_total, $response['data'][0]['purchase_total']);
        self::assertEquals($this->purchase->items_quantity, $response['data'][0]['items_quantity']);
        self::assertEquals([
            [
                'uid' => $this->purchase->uid,
                'date' => $this->purchase->date->format('d-m-Y'),
                'purchase_total' => $this->purchase->purchase_total,
                'items_quantity' => $this->purchase->items_quantity,
                'items' => [
                    [
                        'uid' => $this->purchaseItem1->uid,
                        'name' => $this->purchaseItem1->name,
                        'description' => $this->purchaseItem1->description,
                        'unit_price' => $this->purchaseItem1->unit_price,
                        'total_price' => $this->purchaseItem1->total_price,
                        'quantity' => 1,
                    ],
                    [
                        'uid' => $this->purchaseItem2->uid,
                        'name' => $this->purchaseItem2->name,
                        'description' => $this->purchaseItem2->description,
                        'unit_price' => $this->purchaseItem2->unit_price,
                        'total_price' => $this->purchaseItem2->total_price,
                        'quantity' => 1,
                    ],
                ],
            ]
        ], $response['data']);
    }

    /** @test */
    public function it_eager_loads_the_purchase_items_correctly(): void
    {
        DB::enableQueryLog();
        $this->execute()->assertOk()->json();
        $log = DB::getQueryLog();
        // Assert we have only ran 3 queries.
        // 1 to get the user (authentication)
        // 1 to get the Purchase
        // 1 tot get ALL items for that purchase.
        self::assertCount(3, $log);
    }

    /** @test */
    public function it_denies_access_for_anonymous_user(): void
    {
        $this->actAsAnonymousUser()->execute()->assertForbidden();
    }
}
