<?php

namespace Tests\Unit\Application\Common\Controllers\Auth;

use App\Application\Common\Controllers\Auth\LoginRequest;
use App\Infrastructure\Constants\Cookies;
use Carbon\Carbon;
use Illuminate\Cookie\CookieValuePrefix;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;
use Tymon\JWTAuth\JWT;

class LoginControllerTest extends TestCase
{
    protected string $fullUrl = 'http://api.socialshop.test/login';

    protected string $method = Request::METHOD_POST;

    /** @test */
    public function it_returns_validation_error_without_data(): void
    {
        $this->json($this->method, $this->fullUrl, [])->assertJsonValidationErrors([
            LoginRequest::EMAIL => 'The email field is required',
            LoginRequest::PASSWORD => 'The password field is required',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_with_empty_data(): void
    {
        $data = [
            LoginRequest::EMAIL => '',
            LoginRequest::PASSWORD => '',
        ];

        $this->json($this->method, $this->fullUrl, $data)->assertJsonValidationErrors([
            LoginRequest::EMAIL => 'The email field is required',
            LoginRequest::PASSWORD => 'The password field is required',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_email_address_is_not_an_email_address(): void
    {
        $data = $this->getData();
        $data[LoginRequest::EMAIL] = 'NotAnEmail';

        $this->json($this->method, $this->fullUrl, $data)->assertJsonValidationErrors([
            LoginRequest::EMAIL => 'The email must be a valid email address',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_email_address_is_too_big(): void
    {
        $data = $this->getData();
        $data[LoginRequest::EMAIL] = Str::random(246) . '@gmail.com';

        $this->json($this->method, $this->fullUrl, $data)->assertJsonValidationErrors([
            LoginRequest::EMAIL => 'The email may not be greater than 255 characters',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_email_address_is_not_linked_to_a_user(): void
    {
        $data = $this->getData();
        $data[LoginRequest::EMAIL] = 'other-email@gmail.com';

        $this->json($this->method, $this->fullUrl, $data)->assertJsonValidationErrors([
            LoginRequest::EMAIL => 'The selected email is invalid',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_password_is_too_small(): void
    {
        $data = $this->getData();
        $data[LoginRequest::PASSWORD] = Str::random(7);

        $this->json($this->method, $this->fullUrl, $data)->assertJsonValidationErrors([
            LoginRequest::PASSWORD => 'The password must be at least 8 characters',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_password_is_too_big(): void
    {
        $data = $this->getData();
        $data[LoginRequest::PASSWORD] = Str::random(256);

        $this->json($this->method, $this->fullUrl, $data)->assertJsonValidationErrors([
            LoginRequest::PASSWORD => 'The password may not be greater than 255 characters',
        ]);
    }

    /** @test */
    public function it_returns_validation_error_when_passwords_do_not_match(): void
    {
        $this->json($this->method, $this->fullUrl, $this->getData())->assertJsonValidationErrors([
            LoginRequest::PASSWORD => 'The password is incorrect',
        ]);
    }

    /** @test */
    public function it_queues_the_cookie(): void
    {
        Carbon::setTestNow('2020-11-26 20:00:00');

        $this->user->password = Hash::make('SomeRandomPassword123');
        $this->user->save();

        config()->set('cookies', [
            'name' => 'SocialShop',
            'domain' => 'socialshop.test',
            'secure' => false,
        ]);

        // Cookie and JWT token only have a lifetime of 60 minutes.
        config()->set('jwt.ttl', 60);

        $response = $this->json($this->method, $this->fullUrl, $this->getData());

        $cookies = $response->headers->getCookies();
        self::assertCount(1, $cookies);

        /** @var \Symfony\Component\HttpFoundation\Cookie $cookie */
        $cookie = $cookies[0];
        self::assertNotNull($cookie->getValue());
        // Taylor decided to add a prefix to secure cookies, and added the CookieValuePrefix class in Laravel 8.9.0.
        // @see https://github.com/laravel/framework/commit/edd64e828642c37ed219fc03d7403c77ac1a1909#diff-53ae07068a6109b15bc41214d1d0372d
        $token = CookieValuePrefix::remove(Crypt::decryptString($cookie->getValue()));
        $jwtAuth = app(JWT::class)->setToken($token);
        self::assertEquals('SocialShop', $cookie->getName());
        self::assertEquals('socialshop.test', $cookie->getDomain());
        self::assertEquals('2020-11-26 21:00:00', Carbon::parse($cookie->getExpiresTime())->toDateTimeString());
        self::assertFalse($cookie->isSecure());
        self::assertEquals(Cookies::HTTP_ONLY, $cookie->isHttpOnly());
        self::assertEquals(strtolower(Cookies::SAME_SITE), $cookie->getSameSite());
        self::assertEquals(Cookies::RAW, $cookie->isRaw());
        self::assertEquals(Cookies::PATH ?? '/', $cookie->getPath());
        self::assertEquals($this->user->uid, $jwtAuth->getClaim('sub'));
    }

    private function getData(): array
    {
        return [
            LoginRequest::EMAIL => $this->user->email,
            LoginRequest::PASSWORD => 'SomeRandomPassword123',
        ];
    }

}
