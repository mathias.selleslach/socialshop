<?php

namespace Tests\Unit\Infrastructure\Middleware;

use App\Application\Common\Middleware\AuthorizeHeaders;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;

class AuthorizeHeadersTest extends TestCase
{
    protected AuthorizeHeaders $middleware;

    protected Request $request;

    protected function setUp(): void
    {
        parent::setUp();

        $this->middleware = app(AuthorizeHeaders::class);

        $this->request = new Request();
        $this->request->setMethod(Request::METHOD_GET);
        $this->request->headers->set('Origin', 'http://api.socialshop.test');
        $this->request->headers->set('X-Requested-With', 'XMLHttpRequest');
        $this->request->headers->set('Content-type', 'application/json');
        $this->request->headers->set('Accept', 'application/json');
        $this->request->server->set('REMOTE_ADDR', '192.168.1.1');
        $this->request->setRouteResolver(function (): Route {
            $route = new Route(Request::METHOD_GET, '/test', fn () => null);
            $route->name('test.test');

            return $route;
        });
    }

    /** @test */
    public function it_succeeds_when_method_is_options(): void
    {
        $this->request->setMethod(Request::METHOD_OPTIONS);

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertNull($response);
    }

    /** @test */
    public function it_aborts_mission_when_x_requested_with_header_is_not_set(): void
    {
        $this->request->headers->remove('X-Requested-With');

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) {
            self::assertEquals('Invalid Header X-Requested-With.', $message);
            self::assertEquals([
                'Origin' => 'http://api.socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'GET',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_aborts_mission_when_x_requested_with_header_is_wrong(): void
    {
        $this->request->headers->set('X-Requested-With', 'Boo!');

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) {
            self::assertEquals('Invalid Header X-Requested-With.', $message);
            self::assertEquals([
                'Origin' => 'http://api.socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'GET',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_aborts_mission_when_content_type_header_is_not_set(): void
    {
        $this->request->setMethod(Request::METHOD_POST);
        $this->request->headers->remove('Content-type');

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) {
            self::assertEquals('Invalid Header Content-type.', $message);
            self::assertEquals([
                'Origin' => 'http://api.socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'POST',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_aborts_mission_when_content_type_header_is_wrong(): void
    {
        $this->request->setMethod(Request::METHOD_POST);
        $this->request->headers->set('Content-type', 'Boo!');

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) {
            self::assertEquals('Invalid Header Content-type.', $message);
            self::assertEquals([
                'Origin' => 'http://api.socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'POST',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_aborts_mission_when_accept_header_is_not_set(): void
    {
        $this->request->headers->remove('Accept');

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) {
            self::assertEquals('Invalid Header Accept.', $message);
            self::assertEquals([
                'Origin' => 'http://api.socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'GET',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_aborts_mission_when_accept_header_is_wrong(): void
    {
        $this->request->headers->set('Accept', 'Boo!');

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) {
            self::assertEquals('Invalid Header Accept.', $message);
            self::assertEquals([
                'Origin' => 'http://api.socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'GET',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_succeeds_when_all_headers_are_set_correctly(): void
    {
        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertNull($response);
    }
}
