<?php

namespace Tests\Unit\Infrastructure\Middleware;

use App\Application\Common\Middleware\AuthenticateApi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tests\TestCase;
use Tymon\JWTAuth\JWTAuth;

class AuthenticateApiTest extends TestCase
{
    protected AuthenticateApi $middleware;

    protected Request $request;

    protected function setUp(): void
    {
        parent::setUp();

        $this->middleware = app(AuthenticateApi::class);

        $this->request = new Request();
        $this->request->setMethod(Request::METHOD_GET);
        $this->request->cookies->set(config('cookies.name'), null);
        $this->request->headers->set('Origin', 'http://socialshop.test');
        $this->request->server->set('REMOTE_ADDR', '192.168.1.1');
        $this->request->setRouteResolver(function (): Route {
            $route = new Route(Request::METHOD_GET, '/test', fn () => null);
            $route->name('test.test');

            return $route;
        });
    }

    /** @test */
    public function it_succeeds_when_method_is_options(): void
    {
        $this->request->setMethod(Request::METHOD_OPTIONS);

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertNull($response);
    }

    /** @test */
    public function it_aborts_mission_when_cookie_is_not_found(): void
    {
        $this->request->cookies->set(config('cookies.name'), null);

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) {
            self::assertEquals('Cookie not set.', $message);
            self::assertEquals([
                'Origin' => 'http://socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'GET',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_aborts_mission_when_cookie_contains_invalid_token(): void
    {
        $this->request->cookies->set(config('cookies.name'), 'ThisIsNotACorrectToken');

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) {
            self::assertEquals('JWT Token invalid.', $message);
            self::assertEquals([
                'token' => 'ThisIsNotACorrectToken',
                'Origin' => 'http://socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'GET',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_aborts_mission_with_unauthorized_when_jwt_authentication_check_fails(): void
    {
        $token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.' .
            'eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.' .
            'SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

        $this->request->cookies->set(config('cookies.name'), $token);

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) use ($token) {
            self::assertEquals('JWT authentication check failed.', $message);
            self::assertEquals([
                'token' => $token,
                'Origin' => 'http://socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'GET',
                'Route' => 'test.test',
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /** @test */
    public function it_aborts_mission_when_user_is_not_found(): void
    {
        $uid = $this->user->uid;

        $token = app(JWTAuth::class)->fromUser($this->user);
        $this->request->cookies->set(config('cookies.name'), $token);

        $this->user->forceDelete();

        $this->expectException(HttpException::class);

        Log::shouldReceive('error')->once()->withArgs(function ($message, $context) use ($token, $uid) {
            self::assertEquals('User not found.', $message);
            self::assertEquals([
                'token' => $token,
                'Origin' => 'http://socialshop.test',
                'IP Address' => '192.168.1.1',
                'Method' => 'GET',
                'Route' => 'test.test',
                'sub' => $uid,
            ], $context);

            return true;
        });

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertEquals(Response::HTTP_FORBIDDEN, $response->getStatusCode());
    }

    /** @test */
    public function it_succeeds_when_token_is_set_for_correct_user(): void
    {
        $token = app(JWTAuth::class)->fromUser($this->user);

        $this->request->cookies->set(config('cookies.name'), $token);

        $response = $this->middleware->handle($this->request, fn () => null);

        self::assertNull($response);
        self::assertEquals($this->user->fresh(), Auth::user());
    }
}
