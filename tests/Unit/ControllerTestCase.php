<?php

namespace Tests\Unit;

use App\Domain\Common\User\User;
use Tests\TestCase;
use Illuminate\Testing\TestResponse;
use Tymon\JWTAuth\JWTAuth;

abstract class ControllerTestCase extends TestCase
{
    protected string $routeName = '';

    protected string $redirectRouteName = '';

    protected string $method = '';

    private array $urlParameters = [];

    private array $redirectParameters = [];

    private ?User $jwtUser = null;

    private ?string $jwtToken = null;

    protected $withCredentials = true;

    private bool $actAsAnonymousUser = false;

    protected function execute(
        array $data = null,
        string $uri = null,
        string $method = null,
        array $headers = []
    ): TestResponse {
        $method = $method ?? $this->method;
        $data = $data ?? $this->getData();
        $uri = $uri ?? route($this->routeName, $this->urlParameters ?? []);

        $headers = array_merge($headers, [
            'Origin' => 'http://socialshop.test',
            'Content-type' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest',
            'Accept' => 'application/json'
        ]);

        if ($this->actAsAnonymousUser === true) {
            return $this->json($method, $uri, $data, $headers);
        }

        if ($this->getJwtUser() === null) {
            $this->setJwtUser($this->user);
        }

        $this->withCookie(config('cookies.name'), $this->jwtToken);

        return $this->json($method, $uri, $data, $headers);
    }

    protected function getUrlParameters(): array
    {
        return $this->urlParameters;
    }

    protected function actAsAnonymousUser(): self
    {
        $this->actAsAnonymousUser = true;

        return $this;
    }

    protected function setUrlParameters(array $urlParameters = []): self
    {
        $this->urlParameters = $urlParameters;

        return $this;
    }

    protected function getRedirectParameters(): array
    {
        return $this->redirectParameters;
    }

    protected function setRedirectParameters(array $urlParameters = []): self
    {
        $this->redirectParameters = $urlParameters;

        return $this;
    }

    protected function setJwtUser(User $user): self
    {
        $this->jwtUser = $user;
        $this->jwtToken = app(JWTAuth::class)->fromUser($user);

        return $this;
    }

    protected function getJwtUser(): ?User
    {
        return $this->jwtUser;
    }

    protected function getRedirectUrl(): string
    {
        return route($this->redirectRouteName, $this->getRedirectParameters());
    }

    protected function getData(): array
    {
        return [];
    }
}
