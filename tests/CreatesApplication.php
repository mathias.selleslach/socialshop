<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        if (App::environment() !== 'testing') {
            return $app;
        }

        if (getenv('MYSQL_BOOTSTRAP') !== false) {
            return $app;
        }

        Artisan::call('migrate:fresh');

        putenv('MYSQL_BOOTSTRAP=true');

        return $app;
    }
}
